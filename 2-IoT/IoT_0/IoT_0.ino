/*
 * Questo programma e' il primo della serie per il tutorial IoT
 * del corso di telematica.
 * Potete utilizzarlo anche senza montare il protoshield e
 * l'interfaccia ESP-01. Dopo averlo caricato sull'Arduino, il led
 * montato sull'Arduino iniziera' a lampeggiare.
 * Dopo aver montato il protoshield e l'ESP, collegate 
 * - il pin RXD dell'ESP al pin D0 dell'Arduino
 * - il pin TXD dell'ESP al pin D1 dell'Arduino
 * Configurando opportunamente il Monitor Seriale di Arduino IDE
 * potete inviare comandi AT all'ESP (ad es. AT+GMR, oppure
 * AT+CWLAP)
 */
#define LED 13
#define CHPD 8
void setup() {
  pinMode(LED,OUTPUT);
  pinMode(CHPD,OUTPUT);
  digitalWrite(CHPD,HIGH);
}

void loop() {
  digitalWrite(LED,!(digitalRead(LED)));
  delay(1000);
}
