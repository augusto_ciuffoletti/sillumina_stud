/*
 * Questo programma e' il secondo della serie per il tutorial IoT
 * del corso di telematica.
 * Per utilizzarlo dovete modificare il collegamento di RXD e TXD
 * in modo che siano collegati:
 * - il pin RXD dell'ESP al pin D8 dell'Arduino
 * - il pin TXD dell'ESP al pin D9 dell'Arduino
 * Con le istruzioni "Passo 1" e "Passo 2" commentate, il programma
 * inviera' i due comandi "AT+GMR" e "AT+CWLAP" al ESP-01: utilizzando
 * il Monitor Seriale di Arduino IDE (baudrate 57600) visualizzate
 * la risposta di ESP-01.
 * Abilitando l'istruzione "Passo 1" potete inizializzare il
 * collegamento con il vostro AP. Sostituite il vostro ESSID e PWD
 * nel programma o (meglio) nel file secret.h. Caricate il programma
 * e controllate che l'ESP-01 si agganci al vostro AP.
 * Come ulteriore verificata, commentate nuovamente il "Passo 1" e
 * abilitate l'istruzione "Passo 2". L'ESP-01 eseguira' dei PING
 * sull'host www.google.com, e potrete seguire il risultato sul 
 * Monitor Seriale.
*/

#include "atlib.h"
#include "secret.h"
ESP esp1(10,9,19200,8); // RX,TX,baudrate,CH_PD
void setup() {
  Serial.begin(57600);
}
void loop() {
  char bf[100];
  sprintf(bf,"AT+GMR");
  esp1.atcmd(bf,10);
  sprintf(bf,"AT+CWLAP");
//  sprintf(bf,"AT+CWJAP_DEF=\"%s\",\"%s\"",ESSID,PWD); // Passo 1
//  sprintf(bf,"AT+PING=\"www.google.com\"");           // Passo 2
  esp1.atcmd(bf,10);
  delay(2000);
}
