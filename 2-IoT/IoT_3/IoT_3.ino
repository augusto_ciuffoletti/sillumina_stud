/*
 * Questo programma e' il terzo della serie per il tutorial IoT
 * del corso di telematica.
 * Senza modificare il cablaggio, caricatelo sull'Arduino. Il
 * programma registra un intero crescente sul canale ThingSpeak
 * che avrete creato. Impostate la Write Key del canale nel file
 * secret.h.
*/
#include "secret.h"
#include "atlib.h"
ESP esp1(10,9,19200,8); // RX,TX,baudrate,CH_PD
void setup() {
  Serial.begin(57600);    // Arduino UART baudrate
  esp1.powerUp(30,{});
}
unsigned long n=0;
void read_temp(char temp[]) {
        float r = 10000;
        float a = -19.39;
        float b = 204.7;
        float t = a*log(r/((1024.0/analogRead(A0))-1))+b;
        dtostrf(t, 5, 2, temp);
        return;
}
void loop() {
  char http_buffer[100];
  char temp[6];
  read_temp(temp);
  Serial.println(temp);
  sprintf(http_buffer, "GET /update?api_key=%s&field1=%ld&field2=%s HTTP/1.1\r\n"
                  "Host: api.thingspeak.com\r\n\r\n",
                  CHANNEL_KEY,n++,temp);
  esp1.atcmd("AT+CIPSTART=\"TCP\",\"api.thingspeak.com\",80",5);
  esp1.sendline(http_buffer,5);
  if ( esp1.parsehttp(http_buffer,5) ) { 
    Serial.print("GET successful!\nBody content is: "); 
    Serial.println(http_buffer);
  } else {
    Serial.println("\nPOST failed");
  }
  esp1.readline("CLOSED",5);
  delay(60000); //un dato al minuto
}
