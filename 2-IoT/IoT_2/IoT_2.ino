/*
 * Questo programma e' il terzo della serie per il tutorial IoT
 * del corso di telematica.
 * Senza modificare il cablaggio, caricatelo sull'Arduino. Il
 * programma registra un intero crescente sul canale ThingSpeak
 * che avrete creato. Impostate la Write Key del canale nel file
 * secret.h.
*/
#include "secret.h"
#include "atlib.h"
ESP esp1(10,9,19200,8);   // Costruttore: RX,TX,baudrate,CH_PD
void setup() {
  Serial.begin(57600);    // Impostazione baudrate USB Arduino
  esp1.powerUp(30,{});    // Abilitazione ESP-01: join all'AP impostato con CWJAP_DEF
}
int n=0;
void loop() {
  char http_buffer[100];                                                // Buffer HTTP
  sprintf(http_buffer, "GET /update?api_key=%s&field1=%d HTTP/1.1\r\n"  // HTTP: Impostazione header
                  "Host: api.thingspeak.com\r\n\r\n",
                  CHANNEL_KEY,n++);
  esp1.atcmd("AT+CIPSTART=\"TCP\",\"api.thingspeak.com\",80",5);        // TCP: Connessione HTTP    
  esp1.sendline(http_buffer,5);                             // HTTP: Invio GET REQUEST
  if ( esp1.parsehttp(http_buffer,5) ) {                    // HTTP: Attesa ed elaborazione response1
    Serial.print("GET successful!\nBody content is: "); 
    Serial.println(http_buffer);
  } else {
    Serial.println("\nPOST failed");
  }
  esp1.readline("CLOSED",5);
  delay(60000);           //un dato al minuto
}
