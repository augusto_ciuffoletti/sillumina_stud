# IoT laboratory with Arduino and ESP-01 #

This repo contains 4 sketches that can be used to get a basic understanding of a how an
IoT sensor works, and a few slides to guide the activity.

Using and Arduino UNO (a clone is OK, but do not use Nano or other models) and the ESP_01-based sensor
described [here](https://hackaday.io/project/107721-a-protoshield-as-a-wifi-shield) the student
is guided through a hands on activity that encompasses the wiring of the connection between
the Arduino and the ESP-01, and the publication of data in a ThingSpeak channel.

To complete the activity each student needs the hardware devices needed to implement the sensor, and a computer
with the Arduino IDE. Today, the complete kit costs around 25 Euros, and may require soldering. The kit is
exhaustively described in the slides.

The slides are in Italian: if you are interested, please ask and I'll be happy to translate them
in english (or french).



### Who do I talk to? ###

* augusto@di.unipi.it
* augusto.ciuffoletti@gmail.com