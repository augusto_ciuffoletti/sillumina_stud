import time, sys, httplib, json
if len(sys.argv) != 3: 
  print("\nUsage: ts_talkback.rcv.py <channel id> <api_key>\n")
  exit(1)
channel_id=sys.argv[1] 
api_key=sys.argv[2]
connection = httplib.HTTPSConnection("api.thingspeak.com")              # Connessione
connection.request(                                                     # Request
  "GET",                                                                # - method
  "/talkbacks/"+channel_id+"/commands/execute.json?api_key="+ api_key   # - URL
)
response = connection.getresponse()                                     # Response
print response.status, response.reason
print response.read()                                                   # Il comando estratto
