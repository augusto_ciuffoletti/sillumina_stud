#!/usr/bin/python
import time, random, sys, httplib
if len(sys.argv) != 6:            # Controllo parametri
  print("\nUsage: ts_sensor.py <field#> <min> <max> <deltamax> <apikey>\n")
  exit(1)
fieldname=sys.argv[1]             # acquisizione parametri
minimum=int(sys.argv[2])
maximum=int(sys.argv[3])
deltamax=int(sys.argv[4])
writekey=sys.argv[5]
value=(minimum+maximum)/2         # inizializzazione
while True:                       # loop
  value+=random.randint(-deltamax*10,+deltamax*10)/10.0               # calcolo valore
  if value<minimum: value=minimum
  if value>maximum: value=maximum
  URL="/update?api_key=%s&field%s=%.1f" % (writekey,fieldname,value)  # costruzione URL
  print URL
  connection = httplib.HTTPSConnection("api.thingspeak.com")          # apertura connessione
  connection.request("GET", URL)                                      # invio request
  response = connection.getresponse()                                 # ricezione response
  print response.status, response.reason                              # startline response
  time.sleep(20)
