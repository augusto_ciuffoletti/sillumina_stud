#!/usr/bin/python
import time, sys, httplib
if len(sys.argv) != 2:                                              # Controllo parametri
  print("\nUsage: ts_test.py <writekey>\n")
  exit(1)
writekey=sys.argv[1]
n=0
while True:
  conn = httplib.HTTPSConnection("api.thingspeak.com")              # Apertura connessione
  conn.request("GET","/update?field1="+str(n)+"&api_key="+writekey) # Invio request
  response = conn.getresponse()                                     # Attendo la response
  print response.status, response.reason                            # Stampo l'esito
  time.sleep(20)                                                    # 20 secondi...
  n=n+1
