import serial, time, sys, httplib
if len(sys.argv) != 5:
  print("\nUsage: serial2ts.py <field #> <api_key> <device> <baudrate>\n")
  exit(1)
field=sys.argv[1]
writekey=sys.argv[2]
device=sys.argv[3]
baudrate=int(sys.argv[4])
ser = serial.Serial(device, baudrate, xonxoff=True, rtscts=False, dsrdtr=True)    # Configurazione seriale
ser.flushInput()
ser.flushOutput()
while True:
  data = ser.readline().rstrip()                                                  # Lettura da seriale
  connection = httplib.HTTPSConnection("api.thingspeak.com")                      # Apertura connessione
  connection.request("GET", "/update?api_key="+writekey+"&field"+field+"="+data)  # Feed (con GET)                                      # invio request
  try:
    response = connection.getresponse()                                           # response
    print response.status, response.reason                                        # startline response
    connection.close()
  except:
    print "Connessione fallita: dato non registrato."
