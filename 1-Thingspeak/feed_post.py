import httplib, json
api_key="XXXX"
# Il documento da inserire come oggetto Python (insieme di coppie chiave-valore)
feed = {
  "api_key":api_key,
  "field1":"30"
}
feed_json=json.JSONEncoder().encode(feed)                   # Lo codifico in JSON
conn = httplib.HTTPSConnection("api.thingspeak.com")        # Apro la connessione
conn.request(
  "POST",                                                   # metodo HTTP
  "/update",                                                # URL
  feed_json,                                                # HTTP body
  {"Content-type": "application/json"}                      # HTTP header
)
response = conn.getresponse()
print response.status, response.reason
print response.read()
conn.close()
