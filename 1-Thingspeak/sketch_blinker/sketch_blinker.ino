int n=0;
// Produce il numero di impulsi richiesto
void nblink(int n) {
  for ( int i=0; i<n; i++ ) {
      digitalWrite(13,HIGH);
      delay(200);
      digitalWrite(13,LOW);
      delay(200);
  }
}
void setup() {
  Serial.begin(57600);
  pinMode(13,OUTPUT);
}
void loop() {
  int impulsi=Serial.parseInt();    // riceve dalla seriale
  nblink(impulsi);                  // produce gli impulsi
}
