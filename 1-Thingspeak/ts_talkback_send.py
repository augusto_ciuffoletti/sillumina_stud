import time, sys, httplib, json
if len(sys.argv) != 4:                                                          # Controllo parametri
  print("\nUsage: ts_sensor.py <channel id> <api_key> <command>\n")
  exit(1)
channel_id=sys.argv[1]                                                          # Acquisizione parametri
api_key=sys.argv[2]
command=sys.argv[3]
connection = httplib.HTTPSConnection("api.thingspeak.com")                      # Connessione
connection.request(                                                             # Request
  "POST",                                                                       # - metodo   
  "/talkbacks/"+channel_id+"/commands.json",                                    # - URL
  json.JSONEncoder().encode({ "command_string": command,"api_key": api_key }),  # - body
  {"Content-type": "application/json"}                                          # - headers
)
response = connection.getresponse()                                             # response
print response.status, response.reason
print response.read()
connection.close()
